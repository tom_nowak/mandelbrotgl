Requirements: OpenGL 3, GLEW, GLFW3, GLM  
Building: cmake, make  

User interface:  
keybord arrows - move right/left/up/down  
PageUp - zoom in  
PageDown - zoom out  
