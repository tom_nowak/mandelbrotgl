#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>

class ViewParameters;

class Window
{
public:
    Window(const char *title, float width, float height, ViewParameters &view_parameters);
    void processInput();
    GLFWwindow *getGLFWWindow();

private:
    GLFWwindow* glfwWindow;
    ViewParameters &viewParameters;

    static void framebufferSizeCallback(GLFWwindow* glfw_window, int width, int height);
};