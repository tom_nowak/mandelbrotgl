#pragma once
#include <glm/glm.hpp>

class ViewParameters
{
public:
    enum MoveDirection {LEFT, RIGHT, UP, DOWN};

    ViewParameters(float height_to_width_ratio = 1.0f, float scale = 1.0f, glm::vec2 center = glm::vec2(0.0f, 0.0f));
    const glm::mat3 &getTransformationMatrix() const;
    void updateHeightToWidthRatio(float ratio);
    void move(MoveDirection direction, float coefficient = 0.05f);
    void zoomIn();
    void zoomOut();

private:
    float heightToWidthRatio;
    float scale;
    glm::vec2 center;
    glm::mat3 transformationMatrix;

    void updateMatrix();
};