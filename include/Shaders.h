#pragma once
#include <GL/glew.h>

class ShaderProgram
{
public:
    ShaderProgram();

    void Use() const
    {
        glUseProgram(get_programID());
    }

    GLuint get_programID() const
    {
        return program_id;
    }

private:
    GLuint program_id;
};