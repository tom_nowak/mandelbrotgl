#include <string>
#include <iostream>
#include <stdexcept>
#include <GL/glew.h>
#include <Shaders.h>

constexpr char VERTEX_SHADER[] = R"(#version 330 core
layout (location = 0) in vec2 position;

out vec2 pos;

void main()
{
    gl_Position = vec4(position, 0.0f, 1.0f);
    pos = position;
}
)";

constexpr char FRAGMENT_SHADER[] = R"(#version 330 core
in vec2 pos;

out vec4 outputColor;

precision highp float;
uniform mat3 transform;

vec2 complexMultiply(vec2 z1, vec2 z2);
vec4 calculateColor(vec2 z, int iterations);

void main()
{
	vec2 z = vec2(transform * vec3(pos, 1.0));
	vec2 c = z;
	for(int i=0; i<100; i++) {
		z = complexMultiply(z, z) + c;
		if(dot(z,z) > 4.0) {
            outputColor = calculateColor(z, i);
			return;
		}
	}
	outputColor = vec4(0.0, 0.0, 0.0, 0.0);
}

vec2 complexMultiply(vec2 z1, vec2 z2)
{
    return vec2(z1.x*z2.x - z1.y*z2.y, z1.x*z2.y + z1.y*z2.x);
}

vec4 calculateColor(vec2 z, int iterations)
{
    float colorRegulator = float(iterations - 1) - log2(log2(dot(z, z)));
    vec3 colorHSV = vec3(0.95 + 0.012*colorRegulator, 1.0, 0.2 + 0.4*(1.0 + sin(0.3*colorRegulator)));
	vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
	vec3 m = abs(fract(colorHSV.xxx + K.xyz) * 6.0 - K.www);
	vec4 colorRGBA = vec4(colorHSV.z * mix(K.xxx, clamp(m - K.xxx, 0.0, 1.0), colorHSV.y), 1.0);
    return colorRGBA;
}
)";

namespace
{
GLuint compileShader(const GLchar* shaderCode, GLenum shaderType)
{
    GLuint shader_id = glCreateShader(shaderType);
    glShaderSource(shader_id, 1, &shaderCode, NULL);
    glCompileShader(shader_id);
    GLint success = 0;
    glGetShaderiv(shader_id, GL_COMPILE_STATUS, &success);
    if(!success)
    {
        GLchar infoLog[512];
        glGetShaderInfoLog(shader_id, sizeof(infoLog), NULL, infoLog);
        std::string msg = std::string("Shader compilation: ") + infoLog;
        throw std::runtime_error(msg.c_str());
    }
    return shader_id;
}
}

ShaderProgram::ShaderProgram()
{
    GLuint vertex_id = compileShader(VERTEX_SHADER, GL_VERTEX_SHADER);
    GLuint fragment_id = compileShader(FRAGMENT_SHADER, GL_FRAGMENT_SHADER);
    program_id = glCreateProgram();
    glAttachShader(program_id, vertex_id);
    glAttachShader(program_id, fragment_id);
    glLinkProgram(program_id);
    GLint success;
    glGetProgramiv(program_id, GL_LINK_STATUS, &success);
    if(!success)
    {
        GLchar infoLog[512];
        glGetProgramInfoLog(program_id, sizeof(infoLog), NULL, infoLog);
        std::string msg = std::string("Shader program linking: ") + infoLog;
        throw std::runtime_error(msg.c_str());
    }
    glDeleteShader(vertex_id);
    glDeleteShader(fragment_id);
}
