#include <ViewParameters.h>


ViewParameters::ViewParameters(float height_to_width_ratio, float scale, glm::vec2 center)
    : heightToWidthRatio(height_to_width_ratio), scale(scale), center(center), transformationMatrix(1)
{
    updateMatrix();
}

const glm::mat3 &ViewParameters::getTransformationMatrix() const
{
    return transformationMatrix;
}

void ViewParameters::updateHeightToWidthRatio(float ratio)
{
    heightToWidthRatio = ratio;
    updateMatrix();
}

void ViewParameters::updateMatrix()
{
    transformationMatrix[0][0] = scale;
    transformationMatrix[1][1] = scale * heightToWidthRatio;
    transformationMatrix[2][0] = center.x;
    transformationMatrix[2][1] = center.y;
}

void ViewParameters::move(MoveDirection direction, float coefficient)
{
    switch(direction)
    {
        case LEFT: center.x -= scale*coefficient; updateMatrix(); break;
        case RIGHT: center.x += scale*coefficient; updateMatrix(); break;
        case UP: center.y += scale*coefficient; updateMatrix(); break;
        case DOWN: center.y -= scale*coefficient; updateMatrix(); break;
    }
}

void ViewParameters::zoomIn()
{
    scale /= 1.05f;
    updateMatrix();
}

void ViewParameters::zoomOut()
{
    scale *= 1.05f;
    updateMatrix();
}
