#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <stdexcept>
#include <iostream>
#include <Shaders.h>
#include <Window.h>
#include <ViewParameters.h>

#ifndef NDEBUG
#include <iomanip>

void printMatrix(const glm::mat3 &matrix)
{
    for(int row = 0; row < 3; ++row)
    {
        for(int col = 0; col < 3; ++col)
            std::cout << std::setw(16) << matrix[col][row];
        std::cout << "\n";
    }
    std::cout << "\n\n";
}
#endif

int main()
{
    ViewParameters view_params;
    if(glfwInit() != GL_TRUE)
        throw std::runtime_error("GLFW initialization failed");
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
    try
    {
        Window window("Mandelbrot GL", 800, 600, view_params);

        glewExperimental = GL_TRUE;
        if(glewInit() != GLEW_OK)
            throw std::runtime_error("GLEW Initialization failed");

        ShaderProgram theProgram;

        GLfloat vertices[] =
        {
            -1.0f, -1.0f,
             1.0f, -1.0f,
             1.0f,  1.0f,
            -1.0f,  1.0f,
        };

        GLuint indices[] = {
            0, 1, 2,
            0, 2, 3,
        };

        GLuint VBO, EBO, VAO;
        glGenVertexArrays(1, &VAO);
        glGenBuffers(1, &VBO);
        glGenBuffers(1, &EBO);

        glBindVertexArray(VAO);
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (GLvoid*)0);
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);

        while(!glfwWindowShouldClose(window.getGLFWWindow()))
        {    
            glfwPollEvents();
            window.processInput();
            glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            theProgram.Use();
            glBindVertexArray(VAO);
            GLuint transformLoc = glGetUniformLocation(theProgram.get_programID(), "transform");
            glUniformMatrix3fv(transformLoc, 1, GL_FALSE, glm::value_ptr(view_params.getTransformationMatrix()));
            glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
            glBindVertexArray(0);
            glfwSwapBuffers(window.getGLFWWindow());
#ifndef NDEBUG
            printMatrix(view_params.getTransformationMatrix());
#endif
        }
        glDeleteVertexArrays(1, &VAO);
        glDeleteBuffers(1, &VBO);
        glDeleteBuffers(1, &EBO);
    }
    catch(const std::exception &ex)
    {
        std::cout << ex.what() << std::endl;
        getchar();
    }
    glfwTerminate();
    return 0;
}
