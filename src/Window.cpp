#include <Window.h>
#include <ViewParameters.h>
#include <stdexcept>
#include <iostream>

Window::Window(const char *title, float width, float height, ViewParameters &view_parameters)
    : glfwWindow(glfwCreateWindow(width, height, title, nullptr, nullptr)), viewParameters(view_parameters)
{
    if(glfwWindow == nullptr)
        throw std::runtime_error("GLFW window not created");
    glfwMakeContextCurrent(glfwWindow);
    glfwSetWindowUserPointer(glfwWindow, this);
    glfwSetFramebufferSizeCallback(glfwWindow, framebufferSizeCallback);
    glfwSetWindowSize(glfwWindow, (int)width, (int)height);
}

void Window::processInput()
{
    if(glfwGetKey(glfwWindow, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(glfwWindow, true);
    if(glfwGetKey(glfwWindow, GLFW_KEY_LEFT) == GLFW_PRESS)
        viewParameters.move(ViewParameters::LEFT);
    if(glfwGetKey(glfwWindow, GLFW_KEY_RIGHT) == GLFW_PRESS)
        viewParameters.move(ViewParameters::RIGHT);
    if(glfwGetKey(glfwWindow, GLFW_KEY_UP) == GLFW_PRESS)
        viewParameters.move(ViewParameters::UP);
    if(glfwGetKey(glfwWindow, GLFW_KEY_DOWN) == GLFW_PRESS)
        viewParameters.move(ViewParameters::DOWN);
    if(glfwGetKey(glfwWindow, GLFW_KEY_PAGE_DOWN) == GLFW_PRESS)
        viewParameters.zoomOut();
    if(glfwGetKey(glfwWindow, GLFW_KEY_PAGE_UP) == GLFW_PRESS)
        viewParameters.zoomIn();
}
    
GLFWwindow *Window::getGLFWWindow()
{
    return glfwWindow;
}

void Window::framebufferSizeCallback(GLFWwindow* glfw_window, int width, int height)
{
    std::cout << "resize: " << width << ", " << height << "\n";
    glViewport(0, 0, width, height);
    Window* window = reinterpret_cast<Window*>(glfwGetWindowUserPointer(glfw_window));
    window->viewParameters.updateHeightToWidthRatio((float)height / (float)width);
}